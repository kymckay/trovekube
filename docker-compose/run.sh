#!/usr/bin/env bash

set -eux

for dir in gits gits/git gits/tarballs gits/bundles working-area html db; do
  sudo rm -rf "${dir}"
  mkdir "${dir}"
done

docker-compose build

exec docker-compose up
