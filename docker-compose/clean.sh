#!/usr/bin/env bash

set -eux

for dir in gits working-area html db; do
  sudo rm -rf "${dir}"
done
